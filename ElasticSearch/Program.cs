﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using Common;
using Common.ESModels;
using ElasticSearch.Mapping;

namespace ElasticSearch
{
    public class Program
    {
        private static readonly string MapElasticSearchEndpoint = ConfigurationManager.AppSettings[nameof(MapElasticSearchEndpoint)];
        private static readonly NestService NestService;

        static Program()
        {
            try
            {
                Helper.DefaultConsoleColor();

                Console.WriteLine($"ElasticSearch url: \n{MapElasticSearchEndpoint}\n");
                Thread.Sleep(3000);

                NestService = new NestService(MapElasticSearchEndpoint);
            }
            catch (Exception e)
            {
                Helper.Error(e.Message);
                throw;
            }
        }
        
        static void Main(string[] args)
        {
            try
            {
                Helper.StopWatch(Start);
            }
            catch (Exception e)
            {
                Helper.Error(e.Message);
                throw;
            }

            Console.WriteLine("Completed!");
            Console.ReadLine();
        }

        private static void Start()
        {
            CreateProductIndex();

            CreateShopIndex();

            SendProducts();

            SendShops();

            CreateScriptFieldIsOpen();
        }

        private static void CreateProductIndex()
        {
            var descriptor = ProductIndex.GetIndexDescriptor();
            NestService.CreateIndex(ProductIndex.INDEX_NAME, descriptor);

            Console.WriteLine();
        }

        private static void CreateShopIndex()
        {
            var descriptor = ShopIndex.GetIndexDescriptor();
            NestService.CreateIndex(ShopIndex.INDEX_NAME, descriptor);

            Console.WriteLine();
        }

        private static void SendProducts()
        {
            Console.WriteLine("Start Products:");

            var products = FilesService.GetProducts();
            
            NestService.BulkSend(products, ProductIndex.INDEX_NAME, ProductElastic.MAPPING_NAME);

            Console.WriteLine();
        }

        private static void SendShops()
        {
            Console.WriteLine("Start Shops:");

            var shops = FilesService.GetShops();

            NestService.BulkSend(shops, ShopIndex.INDEX_NAME, ShopElastic.MAPPING_NAME);
            
            Console.WriteLine();
        }

        private static void CreateScriptFieldIsOpen()
        {
            const string scriptName = "shop-is-opened";

            Console.WriteLine($"Start ScriptField - {scriptName}");

            var script = "def timezone = ZoneId.of(doc['timezone'].value);def now = Instant.ofEpochSecond(params.now).atZone(timezone);def todayStart = now.toLocalDate().atStartOfDay(timezone);int day = now.getDayOfWeek().getValue();int prevDay = day == 1 ? 7 : day - 1;def schedule = params['_source']['schedule'][day - 1];def prevSchedule = params['_source']['schedule'][prevDay - 1];for (p in schedule.periods){def s1 = todayStart.plusMinutes(p.shours * 60 + p.sminutes);def e1 = s1.plusMinutes(p.duration);if ((now.isAfter(s1) || now.isEqual(s1)) && (now.isBefore(e1) || now.isEqual(e1))){return true;}}for (p in schedule.periods){def s2 = todayStart.minusDays(1).plusMinutes(p.shours * 60 + p.sminutes);def e2 = s2.plusMinutes(p.duration);if ((now.isAfter(s2) || now.isEqual(s2)) && (now.isBefore(e2) || now.isEqual(e2))){return true;}}return false;";
            var query = "{\"script\":{\"lang\":\"painless\",\"code\":\" " + script + "\"}}";

            NestService.CreateScriptField(query, scriptName);

            Console.WriteLine();
        }
    }
}
