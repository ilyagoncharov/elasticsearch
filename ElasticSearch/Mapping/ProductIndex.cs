﻿using Common.ESModels;
using Nest;

namespace ElasticSearch.Mapping
{
    public class ProductIndex
    {
        public const string INDEX_NAME = "products";

        public static CreateIndexDescriptor GetIndexDescriptor()
        {
            return new CreateIndexDescriptor(INDEX_NAME)
                .Mappings(ms => ms
                    .Map<ProductElastic>(m => m
                        .Properties(ps => ps
                            .Number(v => v
                                .Type(NumberType.Byte)
                                .Name(p => p.Rating))
                            .Number(v => v
                                .Type(NumberType.Long)
                                .Name(p => p.Id))
                            .Text(v => v
                                .Name(p => p.Description))
                            .Text(v => v
                                .Name(p => p.ShortDescription))
                            .Text(v => v
                                .Name(p => p.Name)
                                .Fields(p => p.Keyword(k => k.Name("keyword"))))
                            .Number(v => v
                                .Type(NumberType.Long)
                                .Name(p => p.ShopId))
                            .Text(v => v
                                .Name(p => p.ShopName))
                            .Number(v => v
                                .Type(NumberType.Long)
                                .Name(p => p.CategoryId))
                            .Text(v => v
                                .Name(p => p.CategoryName)
                                .Fields(p => p.Keyword(k => k.Name("keyword"))))
                            .Number(v => v
                                .Type(NumberType.Integer)
                                .Name(p => p.DisplayOrder))
                            .Nested<string>(v => v
                                .Name(p => p.Tags))
                            .Nested<string>(v => v
                                .Name(p => p.ImageUrls))
                            .Object<ProductPriceElastic>(v => v
                                .Name(p => p.Prices)
                                .Properties(eps => eps
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.gram))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.two_grams))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.eighth))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.quarter))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.half_ounce))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.ounce))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.unit))
                                    .Number(vs => vs
                                        .Type(NumberType.Double)
                                        .Name(p => p.half_gram))
                                )
                            )
                        )
                    )
                );
        }
    }
}