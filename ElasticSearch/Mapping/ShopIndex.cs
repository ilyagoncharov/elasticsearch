﻿using Common.ESModels;
using Nest;

namespace ElasticSearch.Mapping
{
    public static class ShopIndex
    {
        public const string INDEX_NAME = "shops";

        public static CreateIndexDescriptor GetIndexDescriptor()
        {
            return new CreateIndexDescriptor(INDEX_NAME)
                .Mappings(ms => ms
                    .Map<ShopElastic>(m => m
                        .Properties(ps => ps
                            .Number(v => v
                                .Type(NumberType.Long)
                                .Name(p => p.Id))
                            .Text(v => v
                                .Name(p => p.Name)
                                .Fields(p => p.Keyword(k => k.Name("keyword"))))
                            .Keyword(v => v
                                .Name(p => p.Type))
                            .Text(v => v
                                .Name(p => p.Address))
                            .Text(v => v
                                .Name(p => p.Website))
                            .Text(v => v
                                .Name(p => p.Description))
                            .Text(v => v
                                .Name(p => p.Phone1))
                            .Text(v => v
                                .Name(p => p.Phone2))
                            .Number(v => v
                                .Type(NumberType.Double)
                                .Name(p => p.Rating))
                            .Number(v => v
                                .Type(NumberType.Integer)
                                .Name(p => p.Reviews))
                            .Keyword(v => v
                                .Name(p => p.Timezone))
                            .GeoPoint(v => v
                                .Name(p => p.Location))
                            .Nested<ShopScheduleDayElastic>(v => v
                                .Name(p => p.Schedule)
                                .Properties(eps => eps
                                    .Number(s => s
                                        .Type(NumberType.Byte)
                                        .Name(p => p.Day))
                                    .Nested<ShopScheduleDayPeriodElastic>(vc => vc
                                        .Name(p => p.Periods)
                                        .Properties(epc => epc
                                            .Number(vs => vs
                                                .Type(NumberType.Integer)
                                                .Name(p => p.Duration))
                                            .Number(vs => vs
                                                .Type(NumberType.Byte)
                                                .Name(p => p.StartHours))
                                            .Number(vs => vs
                                                .Type(NumberType.Byte)
                                                .Name(p => p.StartMinutes))
                                        )
                                    )
                                )
                            )
                        )
                    )
                );
        }
    }
}