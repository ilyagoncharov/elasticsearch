﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Common;
using Common.ESModels;
using ElasticSearch.Mapping;
using Nest;

namespace ElasticSearch
{
    public class NestService
    {
        private readonly ElasticClient _client;
        private string Url { get; }

        public NestService(string url)
        {
            Url = url;

            var settings = new ConnectionSettings(new Uri(Url))
                .MapDefaultTypeIndices(m =>
                {
                    m.Add(typeof(ShopElastic), ShopIndex.INDEX_NAME);
                    m.Add(typeof(ShopScheduleDayElastic), ShopIndex.INDEX_NAME);
                    m.Add(typeof(ShopScheduleDayPeriodElastic), ShopIndex.INDEX_NAME);

                    m.Add(typeof(ProductElastic), ProductIndex.INDEX_NAME);
                    m.Add(typeof(ProductPriceElastic), ProductIndex.INDEX_NAME);
                }).MapDefaultTypeNames(m =>
                {
                    m.Add(typeof(ShopElastic), ShopElastic.MAPPING_NAME);
                    m.Add(typeof(ProductElastic), ProductElastic.MAPPING_NAME);
                })
                .EnableDebugMode();

            _client = new ElasticClient(settings);

            CheckConnenct();
        }

        public void CheckConnenct()
        {
            var pingResponse = _client.Ping();
            if (!pingResponse.IsValid)
            {
                throw new Exception(pingResponse.DebugInformation);
            }
        }

        public bool IndexIsExist(string index)
        {
            return _client.IndexExists(index).Exists;
        }

        public void CreateIndex(string index, CreateIndexDescriptor descriptor)
        {
            var isExist = IndexIsExist(index);

            if (isExist) return;

            var createIndexResponse = _client.CreateIndex(descriptor);

            if (createIndexResponse.IsValid)
            {
                Console.WriteLine($"Index {index} created.");
            }
            else
            {
                throw new Exception(createIndexResponse.DebugInformation);
            }
        }

        public void BulkSend<T>(List<T> objects, string index, string type = null) where T : class
        {
            var total = 0;
            const int step = 1000;
            for (var i = 0; i < objects.Count; i = i + step)
            {
                var list = objects.Skip(i).Take(step).ToList();

                Send(list);

                total += list.Count;

                Console.Write($"\rSending... Processed: {Helper.GetPercent(objects.Count, total)}%. Sent objects: {total}. Total objects: {objects.Count}.");
            }

            Console.WriteLine();

            void Send(List<T> list)
            {
                var descriptor = new BulkDescriptor();

                foreach (var item in list)
                {
                    var _type = type ?? index;
                    descriptor.Index<T>(x => x.Document(item).Index(index).Type(_type));
                }

                var result = _client.Bulk(descriptor);

                if (!result.IsValid)
                {
                    throw new Exception(result.DebugInformation);
                }
            }
        }

        public void CreateScriptField(string data, string scriptName)
        {
            var subUrl = $"/_scripts/{scriptName}";

            Task.Run(() => Http.SendPost(Url, subUrl, data)).Wait();
        }
    }
}
