﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Common;
using Common.ESModels;
using Nest;
using Newtonsoft.Json;
using RestSharp;
using Weedmaps.Shops.Models;

namespace Weedmaps.Shops
{
    public class Program
    {
        static Program()
        {
            Helper.DefaultConsoleColor();
        }

        static void Main(string[] args)
        {
            Start();

            Console.ReadKey();
        }

        public static void Start()
        {
            var types = new[] { "dispensaries", "deliveries", "doctors" };

            var list = new List<ShopElastic>();

            foreach (var type in types)
            {
                var page = 1;
                var pageSize = 150; // 150 is max page size for weed maps
                var response = GetWeedMapsPage(page, pageSize, type);
                var total = response.meta.total_listings;

                var items = ParseWeedMapsResponse(response, type);
                list.AddRange(items);

                while (total > page * pageSize)
                {
                    page++;
                    response = GetWeedMapsPage(page, pageSize, type);

                    var items1 = ParseWeedMapsResponse(response, type);
                    list.AddRange(items1);

                    Console.Write($"\rUploaded WeedMaps: {list.Count}");
                }
            }
            Console.WriteLine();

            var json = JsonConvert.SerializeObject(list, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            FilesService.SaveShopFile($"shops", json);
        }

        private static WeedMapsResponse GetWeedMapsPage(int page, int pageSize, string type)
        {
            var client = new RestClient("https://api-v2.weedmaps.com/api/v2");
            var request = new RestRequest("listings");
            request.AddQueryParameter("filter[plural_types][]", type);
            request.AddQueryParameter("page_size", pageSize.ToString());
            request.AddQueryParameter("page", page.ToString());

            var response = client.Execute<WeedMapsResponse>(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return new WeedMapsResponse();
            }

            return client.Execute<WeedMapsResponse>(request).Data;
        }

        private static List<ShopElastic> ParseWeedMapsResponse(WeedMapsResponse response, string type)
        {
            var list = new List<ShopElastic>();

            var listings = response?.data?.listings ?? new List<Listing>();
            foreach (var listing in listings)
            {
                try
                {
                    var obj = new ShopElastic
                    {
                        Id = listing.id,
                        Name = listing.name,
                        Description = listing.intro_body,
                        Type = type,
                        Address = listing.address + "<br>" + listing.city + ", " + listing.zip_code,
                        Website = listing.web_url,
                        Phone1 = "(303) 623-3414",
                        Phone2 = "(303) 575-6753",
                        Rating = listing.rating,
                        Reviews = listing.reviews_count,
                        Location = new GeoLocation(listing.latitude, listing.longitude),
                        Timezone = listing.timezone,
                        Schedule = GenerateSchedule()
                    };

                    list.Add(obj);
                }
                catch
                {
                    Console.WriteLine($"{listing.name} {listing.latitude},{listing.longitude} isn't created");
                }
            }

            return list;
        }

        private static List<ShopScheduleDayElastic> GenerateSchedule()
        {
            var schedule = new List<ShopScheduleDayElastic>(7);
            var random = new Random();
            var isoDay = 1;
            foreach (var day in Enum.GetValues(typeof(DayOfWeek)))
            {
                var isWorked = random.Next(0, 999) % 2 == 0;

                var start_hour = random.Next(8, 11);
                var start_minutes = random.Next(0, 59);
                var end_hour = random.Next(16, 19);
                var end_minutes = random.Next(0, 59);

                var duration = (end_hour * 60 + end_minutes) - (start_hour * 60 + start_minutes);

                schedule.Add(new ShopScheduleDayElastic
                {
                    Day = Convert.ToByte(isoDay),
                    Periods = isWorked
                    ? new List<ShopScheduleDayPeriodElastic> { new ShopScheduleDayPeriodElastic
                        {
                            StartHours = Convert.ToByte(start_hour),
                            StartMinutes = Convert.ToByte(start_minutes),
                            Duration = duration
                        } }
                    : new List<ShopScheduleDayPeriodElastic>(0)
                });
                isoDay++;
            }
            
            return schedule;
        }
    }
}
