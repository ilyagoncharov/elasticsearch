﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weedmaps.Shops.Models
{
    public class WeedMapsResponse
    {
        public Meta meta { get; set; }
        public Data data { get; set; }
    }

    public class Meta
    {
        public int total_listings { get; set; }
    }

    public class Data
    {
        public List<Listing> listings { get; set; }
    }

    public class AvatarImage
    {
        public string small_url { get; set; }
    }

    public class OnlineOrdering
    {
        public bool enabled_for_pickup { get; set; }
        public bool enabled_for_delivery { get; set; }
    }
}
