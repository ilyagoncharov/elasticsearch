﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weedmaps.Shops.Models
{
    public class Listing
    {
        public long id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string type { get; set; }
        public int wmid { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string web_url { get; set; }
        public string package_level { get; set; }
        public int feature_order { get; set; }
        public double ranking { get; set; }
        public double rating { get; set; }
        public int reviews_count { get; set; }
        public AvatarImage avatar_image { get; set; }
        public string license_type { get; set; }
        public string address { get; set; }
        public object distance { get; set; }
        public string zip_code { get; set; }
        public string timezone { get; set; }
        public string intro_body { get; set; }
        public string static_map_url { get; set; }
        public bool open_now { get; set; }
        public int? closes_in { get; set; }
        public string todays_hours_str { get; set; }
        public int? min_age { get; set; }
        public int menu_items_count { get; set; }
        public int verified_menu_items_count { get; set; }
        public int endorsement_badge_count { get; set; }
        public bool is_published { get; set; }
        public OnlineOrdering online_ordering { get; set; }
    }
}
