﻿using System;
using System.Collections.Generic;
using Nest;
using Newtonsoft.Json;

namespace Common.ESModels
{
    public class ShopElastic
    {
        public const string MAPPING_NAME = "shop";

        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Website { get; set; }
        public string Type { get; set; }
        public string Timezone { get; set; }
        public double Rating { get; set; }
        public int Reviews { get; set; }
        public GeoLocation Location { get; set; }
        public List<ShopScheduleDayElastic> Schedule { get; set; }
    }
    
    public class ShopScheduleDayElastic
    {
        public byte Day { get; set; }
        public List<ShopScheduleDayPeriodElastic> Periods { get; set; }
    }

    public class ShopScheduleDayPeriodElastic
    {
        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("shours")]
        public byte StartHours { get; set; }

        [JsonProperty("sminutes")]
        public byte StartMinutes { get; set; }
    }
}
