﻿using System.Collections.Generic;

namespace Common.ESModels
{
    public class ProductElastic
    {
        public const string MAPPING_NAME = "product";

        public int Id { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public List<string> ImageUrls { get; set; }
        public string ImageUrl { get; set; }
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long Rating { get; set; }
        public List<object> Tags { get; set; }
        public ProductPriceElastic Prices { get; set; }


        //public string Country { get; set; }
        //public string Subregion { get; set; }
        //public string Shop { get; set; }
        //public string Category { get; set; }
        //public int? strain_id { get; set; }
        //public string license_type { get; set; }
        //public string grams_per_eighth { get; set; }
        //public int listing_id { get; set; }
        //public string listing_type { get; set; }
        //public int vendor_id { get; set; }
        //public string vendor_name { get; set; }
        //public string vendor_slug { get; set; }
        //public string slug { get; set; }
        //public bool published { get; set; }
        //public string listing_name { get; set; }
        //public bool approved_endorsement { get; set; }
        //public int sort_priority { get; set; }
    }

    public class ProductPriceElastic
    {
        public double gram { get; set; }
        public double two_grams { get; set; }
        public double eighth { get; set; }
        public double quarter { get; set; }
        public double half_ounce { get; set; }
        public double ounce { get; set; }
        public double? unit { get; set; }
        public double? half_gram { get; set; }
    }
}
