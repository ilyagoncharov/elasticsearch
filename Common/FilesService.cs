﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.ESModels;
using Ionic.Zip;
using Ionic.Zlib;
using Newtonsoft.Json;
using GZipStream = System.IO.Compression.GZipStream;

namespace Common
{
    public static class FilesService
    {
        private const string ProductFolderName = "Products";
        private const string ShopFolderName = "Shops";

        private static string GetFilesDirectory()
        {
            var directory1 = Directory.GetCurrentDirectory();
            var directory2 = Path.GetDirectoryName(directory1);
            var directory3 = Path.GetDirectoryName(directory2);
            var directory4 = Path.GetDirectoryName(directory3);
            var directory5 = directory4 + "\\ElasticSearch\\bin\\Debug";
            return directory5;
        }

        private static void ClearOrCreateDirectory(string directory)
        {
            var exists = Directory.Exists(directory);
            if (!exists)
            {
                Directory.CreateDirectory(directory);
            }
            else
            {
                var di = new DirectoryInfo(directory);

                foreach (var file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (var dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
        }

        private static void SaveFile(string folderName, string fileName, string body)
        {
            var directory = $"{GetFilesDirectory()}\\{folderName}";

            //ClearOrCreateDirectory(directory);
            
            var filePath = $"{directory}\\{fileName}.json";
            var zipPath = $"{directory}\\{fileName}.zip";

            using (var st = new StreamWriter(filePath))
            {
                st.Write(body);
            }

            using (var zip = new ZipFile())
            {
                zip.CompressionLevel = CompressionLevel.BestCompression;
                zip.AddFile(filePath, "archive");
                zip.Save(zipPath);
            }
            
            File.Delete(filePath);

            Console.WriteLine($"Created file: \n{zipPath}");
        }

        public static void SaveProductFile(string fileName, string body)
        {
            SaveFile(ProductFolderName, fileName, body);
        }

        public static void SaveShopFile(string fileName, string body)
        {
            SaveFile(ShopFolderName, fileName, body);
        }

        private static List<T> GetFilesData<T>(string folderName)
        {
            var list = new List<T>();

            var directory = Directory.GetCurrentDirectory() + $"\\{folderName}";
            var pathsAllFiles = Directory.GetFiles(directory);

            if (pathsAllFiles.Length == 0)
            {
                Helper.Error($"Directory: {directory} is empty.");
                return list;
            }

            foreach (var path in pathsAllFiles)
            {
                var json = Path.GetExtension(path) == ".zip" ? Unzip(path) : File.ReadAllText(path);
                
                if (!string.IsNullOrEmpty(json))
                {
                    var models = JsonConvert.DeserializeObject<List<T>>(json);
                    list.AddRange(models);
                }
            }

            if (!list.Any())
            {
                Helper.Error("No data.");
            }

            return list;

            string Unzip(string filePath)
            {
                string result;

                using (var zip = ZipFile.Read(filePath))
                using (var stream = new MemoryStream())
                {
                    foreach (var e in zip)
                    {
                        e.Extract(stream);
                    }
                    stream.Position = 0;

                    using (var read = new StreamReader(stream))
                    {
                        result = read.ReadToEnd();
                    }
                }

                return result;
            }
        }

        public static List<ProductElastic> GetProducts()
        {
            return GetFilesData<ProductElastic>(ProductFolderName);
        }

        public static List<ShopElastic> GetShops()
        {
            return GetFilesData<ShopElastic>(ShopFolderName);
        }
    }
}
