﻿using System;
using System.Diagnostics;

namespace Common
{
    public class Helper
    {
        public static int GetPercent(int maximum, int current)
        {
            return (int)Math.Round((double)(100 * current) / maximum);
        }

        public static void StopWatch(Action method)
        {
            var stopWatch = new Stopwatch();

            stopWatch.Start();

            method();

            stopWatch.Stop();

            var ts = stopWatch.Elapsed;
            var elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
            Console.WriteLine("RunTime " + elapsedTime);
        }

        public static void DefaultConsoleColor()
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }

        public static void Error(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            
            const int count = 500;
            var messageLength = message.Length;
            var length = messageLength < count ? messageLength : count;
            var str = message.Substring(0, length);
            Console.WriteLine(str);

            DefaultConsoleColor();
        }
    }
}
