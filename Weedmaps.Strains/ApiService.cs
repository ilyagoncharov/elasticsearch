﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Weedmaps.Products.Models;

namespace Weedmaps.Products
{
    public class BaseApiService
    {
        protected static string RestSend(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            return response.Content;
        }
    }

    public class ApiServiceService : BaseApiService
    {
        // Получить список городов
        public static List<Subregion> GetSubregions(string country)
        {
            var url = "https://api-g.weedmaps.com/wm/v1/regions/" + country + "/subregions";
            var content = RestSend(url);

            var objContent = JObject.Parse(content);
            var objData = (JObject)objContent["data"];
            var objSubregions = objData["subregions"];

            return objSubregions.ToObject<List<Subregion>>();
        }

        // Получить список магазинов города
        public static List<Shop> GetSubregionShops(string subregionSlug)
        {
            var url =
                "https://api-g.weedmaps.com/wm/v2/listings?filter%5Bplural_types%5D%5B%5D=dispensaries&filter%5Bregion_slug%5Bdispensaries%5D%5D=" +
                subregionSlug + "&page_size=100&size=100";
            var content = RestSend(url);

            var joResponse = JObject.Parse(content);
            var objData = (JObject)joResponse["data"];
            var objShops = objData["listings"];

            return objShops.ToObject<List<Shop>>();
        }

        // Получить список продуктов магазина
        public static Shop GetShopProducts(string shopSlug)
        {
            var url = "https://weedmaps.com/api/web/v1/listings/" + shopSlug + "/menu?type=dispensary";
            var content = RestSend(url);

            var result = JsonConvert.DeserializeObject<Shop>(content);

            return result;
        }
    }
}
