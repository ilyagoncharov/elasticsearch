﻿using System;
using System.Collections.Generic;
using System.Linq;
using Weedmaps.Products.Models;

namespace Weedmaps.Products
{
    public class Service
    {
        public static Country USA(string country)
        {
            Console.WriteLine($"Start: {country}");

            var allCities = new List<Subregion>();

            var states = ApiServiceService.GetSubregions(country);

            foreach (var state in states)
            {
                var subregions = ApiServiceService.GetSubregions(state.slug);
                var take = subregions.Take(300);
                allCities.AddRange(take);
            }

            foreach (var city in allCities)
            {
                try
                {
                    var shops = ApiServiceService.GetSubregionShops(city.slug);
                    city.shops.AddRange(shops);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            var result = ItemsSave(allCities);

            var countryObj = new Country
            {
                slug = country,
                Subregions = result
            };

            Console.WriteLine();
            return countryObj;
        }

        public static Country Country(string country)
        {
            Console.WriteLine($"Start: {country}");

            var subregions = СountrySave(country);
            var subregions2 = ItemsSave(subregions);

            var countryObj = new Country
            {
                slug = country,
                Subregions = subregions2
            };

            Console.WriteLine();
            return countryObj;
        }

        private static List<Subregion> СountrySave(string country)
        {
            var subregions = ApiServiceService.GetSubregions(country);

            foreach (var subregion in subregions)
            {
                try
                {
                    var shops = ApiServiceService.GetSubregionShops(subregion.slug);
                    subregion.shops.AddRange(shops);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return subregions;
        }

        private static List<Subregion> ItemsSave(List<Subregion> subregions)
        {
            var subregionsResult = new List<Subregion>();

            Console.WriteLine($"Total cities: {subregions.Count}");

            for (var i = 0; i < subregions.Count; i++)
            {
                try
                {
                    var subregion = subregions[i];
                    var subregionResult = new Subregion
                    {
                        id = subregion.id,
                        name = subregion.name,
                        slug = subregion.slug,
                        region_path = subregion.region_path
                    };

                    foreach (var shop in subregion.shops)
                    {
                        var shopDetails = ApiServiceService.GetShopProducts(shop.slug);

                        if (shopDetails.total_items > 0)
                        {
                            shopDetails.id = shop.id;
                            shopDetails.name = shop.name;
                            shopDetails.slug = shop.slug;
                            subregionResult.shops.Add(shopDetails);
                        }
                    }

                    if (subregionResult.total_items > 0)
                    {
                        subregionsResult.Add(subregionResult);

                        Console.WriteLine($"i: {i}. {subregionResult.name}");
                    }
                    else
                    {
                        Console.WriteLine($"i: {i}. -");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return subregionsResult;
        }
    }
}
