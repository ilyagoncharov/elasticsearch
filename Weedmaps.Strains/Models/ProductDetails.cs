﻿using Common.ESModels;

namespace Weedmaps.Products.Models
{
    public class ProductDetails : ProductElastic
    {
        public ProductDetails(Item item, Category category, Shop shop, Subregion subregion, Country country, int rating)
        {
            Id = item.id;
            Name = item.name;
            Description = item.body;
            ImageUrl = item.image_url;
            ShopId = shop.id;
            ShopName = shop.name;
            CategoryId = item.category_id;
            CategoryName = item.category_name;
            Rating = rating;
            Tags = item.tags;
            Prices = item.prices;


            //Country = country.slug;
            //Subregion = subregion.name;
            //Shop = shop.name;
            //Category = category.title;

            //strain_id = item.strain_id;
            //license_type = item.license_type;
            //grams_per_eighth = item.grams_per_eighth;
            //license_type = item.license_type;
            //vendor_id = item.vendor_id;
            //vendor_name = item.vendor_name;
            //vendor_slug = item.vendor_slug;
            //slug = item.slug;
            //published = item.published;
            //listing_name = item.listing_name;
            //approved_endorsement = item.approved_endorsement;
            //sort_priority = item.sort_priority;
        }
    }
}
