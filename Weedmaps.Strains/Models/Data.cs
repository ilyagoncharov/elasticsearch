﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.ESModels;
using Newtonsoft.Json;

namespace Weedmaps.Products.Models
{
    public class Data
    {
        public Data()
        {
            Countries = new List<Country>();
        }

        public List<Country> Countries { get; set; }
    }

    public class Country
    {
        public Country()
        {
            Subregions = new List<Subregion>();
        }

        public string slug { get; set; }
        public List<Subregion> Subregions { get; set; }
    }

    public class Subregion
    {
        public Subregion()
        {
            shops = new List<Shop>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string region_path { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int total_items => shops.Sum(x => x.total_items);
        public List<Shop> shops { get; set; }
    }

    public class Shop
    {
        public Shop()
        {
            categories = new List<Category>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int total_items => categories.SelectMany(x => x.items).Count();
        public List<Category> categories { get; set; }
    }

    public class Category
    {
        public string title { get; set; }
        public int menu_item_category_id { get; set; }
        public List<Item> items { get; set; }
    }

    public class Item
    {
        public int id { get; set; }
        public string body { get; set; }
        public string name { get; set; }
        public int? strain_id { get; set; }
        public string license_type { get; set; }
        public string grams_per_eighth { get; set; }
        public int listing_id { get; set; }
        public string listing_type { get; set; }
        public int vendor_id { get; set; }
        public string vendor_name { get; set; }
        public string vendor_slug { get; set; }
        public ProductPriceElastic prices { get; set; }
        public string image_url { get; set; }
        public int category_id { get; set; }
        public string category_name { get; set; }
        public List<object> tags { get; set; }
        public string slug { get; set; }
        public bool published { get; set; }
        public string listing_name { get; set; }
        public bool approved_endorsement { get; set; }
        public int sort_priority { get; set; }
    }
}
