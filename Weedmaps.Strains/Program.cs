﻿using System;
using System.Collections.Generic;
using Common;
using Newtonsoft.Json;
using Weedmaps.Products.Models;

namespace Weedmaps.Products
{
    public class Program
    {
        private static readonly List<string> Countries = new List<string>
        {
            "switzerland",
            "germany",
            "spain",
            "canada"
        };

        static Program()
        {
            Helper.DefaultConsoleColor();
        }

        static void Main(string[] args)
        {
            Helper.StopWatch(Start);

            Console.ReadKey();
        }

        private static void Start()
        {
            var countryList = new List<Country>();

            Countries.ForEach(countryName =>
            {
                var country = Service.Country(countryName);
                countryList.Add(country);
            });
            var usa = Service.USA("united-states");
            countryList.Add(usa);

            var result = new Data
            {
                Countries = countryList
            };
            
            var listResult = new List<ProductDetails>();
            var rand = new Random();
            var postCount = 0;
            var count = 0;

            foreach (var country in result.Countries)
                foreach (var subregion in country.Subregions)
                    foreach (var shop in subregion.shops)
                        foreach (var category in shop.categories)
                            foreach (var item in category.items)
                            {
                                var requestData = new ProductDetails(item, category, shop, subregion, country, rand.Next(61, 96));
                                listResult.Add(requestData);

                                if (count++ > 100000)
                                {
                                    Save();
                                }
                            }

            if (count > 0)
            {
                Save();
            }

            void Save()
            {
                count = 0;
                var json = JsonConvert.SerializeObject(listResult, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                FilesService.SaveProductFile($"products{++postCount}", json);
                listResult = new List<ProductDetails>();
            }
        }
    }
}
